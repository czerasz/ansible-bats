# Ansible Bats Role

A role used to install bats the bash testing framework.

## Installation

Install the role with:

```
git clone https://gitlab.com/czerasz/ansible-bats.git /etc/ansible/roles/czerasz.bats
```

## Usage

Usage examplle:

```
- name: Install bats testing framework
  hosts: all
  roles:
    - role: czerasz.bats
      vars:
        version: 0.4.0
```

# Test

Requirements:

- [Docker Compose](https://docs.docker.com/compose/)

Run tests with:

```
docker-compose -f test/docker-compose.yml up
```
