---

- name: Check if bats testing framework is already installed
  stat:
    path: /usr/local/bin/bats
  register: bats_bin_file

- name: Set bats_version to 0.0.0 if bats testing framework doesn't exists
  set_fact:
    bats_version: "0.0.0"
  when: not bats_bin_file.stat.exists

- name: Get current bats testing framework version
  shell: |
    /usr/local/bin/bats --version | sed "s/^Bats\s\([0-9]\+\.[0-9]\+\.[0-9]\+\).*/\1/"
  # `/usr/local/bin/bats --version` returns Bats 0.4.0
  #
  # The command returns a version string
  # Example: { "stdout": "0.4.0", ... }
  register: bats_current_version
  when: bats_bin_file.stat.exists

- name: Set git_installed_version fact
  set_fact:
    bats_version: "{{ bats_current_version.stdout }}"
  when: bats_bin_file.stat.exists

- name: View current bats testing framework version
  debug:
    msg: "{{ bats_version }}"

# Install bats testing framework if it doesn't exist or the version doesn't match
- block:
  - name: Download the bats test framework archive
    unarchive:
      src: "https://github.com/sstephenson/bats/archive/v{{ version }}.tar.gz"
      dest: "/tmp"
      remote_src: yes
      creates: "/tmp/bats-{{ version }}"

  - name: Install the bats framework
    command: "./install.sh /usr/local"
    args:
      chdir: "/tmp/bats-{{ version }}"

  - name: Remove temporary files
    file:
      path: "/tmp/bats-{{ version }}"
      state: absent
  when: bats_version | version_compare(version, '!=')
