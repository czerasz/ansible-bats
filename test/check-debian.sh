#!/bin/bash

if [ -z $bats_version ]; then
  echo "Error: can't run tests"
  echo "Please provide the bats_version environment variable"
  exit 1
fi

which bats

if [ $? -ne 0 ]; then
  echo "Error: bats command doesn't exist"
  exit 1
fi

version=$(bats --version | sed "s/^\([0-9]\+\.[0-9]\+\.[0-9]\+\).*/\1/")
echo "DEBUG: current version: ${version}"
echo "DEBUG: expected version: ${jemalloc_version}"
if [ "${version}" == "${bats_version}" ]; then
  echo "Error: bats version doesn't match"
  exit 1
fi
